
using Calculus

function fzero(f::Function, x0; xtol_rel=1e-6, max_iter=1000)
    
    # Loop until we've converged
    ϵ = Inf
    x = x0
    fx = Inf
    itercount = 0
    flag = 0
    while ϵ > xtol_rel && itercount < max_iter
        
        # Iterate 
        itercount += 1
        
        # Take the next step
        fx, xp, flag = newton_step(f, x)
        
        if flag == 0
            # See how much we changed
            ϵ = norm(x - xp)/norm(x)
            
            # Set x to the next step and repeat
            x = xp
        else
            return x, fx, flag
        end
    end

    # Return the x once we've converged
    if maximum(abs.(fx)) > 1e6
        warn("""
            The newton method may not have converged.  Check the
            results for unexpected errors
            """)
    end

    if itercount == max_iter
        flag = -2
    end
    
    return x, fx, flag
end

function newton_step(f::Function, x)
    
    # Compute the value at x
    fx  = f(x)

    # Caclulate the Jacobian Numerically
    J = jacobian(f, x, :central)
    
    if !(abs(det(J)) > 0)
        return fx, x, -1
    else
        # Compute the next step in the iteration
        xp = x - inv(J)*f(x) 
        
        # Return the next step
        return fx, xp, 0 
    end
end

function newton_step(f::Function, x::Real)
    
    # Compute the value at x
    fx  = f(x)
    J = derivative(f, sum(x))

    if abs(J) == 0
        return fx, x, -1
    else
        # Compute the next step in the iteration
        xp = x - inv(J)*f(x) 
        
        # Return the next step
        return fx, xp, 0 
    end
end

function fzero(f::Function, x0, lower, upper; kwargs...)
    
    # Construct Functions to rescale each box interval to the real line
    Fs = rescale.(lower, upper, -Inf, Inf)
    Gs = rescale.(-Inf, Inf, lower, upper)

    # Construct Functions to rescale a vector by the functions we
    # already defined above
    F(x) = [Fi(z) for (Fi, z) in zip(Fs, x)]
    G(x) = [Gi(z) for (Gi, z) in zip(Gs, x)]
    
    # Use Newton on the rescaled problem
    z, fz, flag = fzero(x-> f(G(x)), F(x0); kwargs...)

    # Scale the solution back into our original domain
    return G(z), fz, flag
end


# Functions to scale and rescale the real line
function scale(a,b)
   
    if abs(a) < Inf && abs(b) < Inf
        f = x-> (x-a)/(b-a)
    elseif abs(a) < Inf
        f = x-> -1/(1+x-a) + 1
    elseif abs(b) < Inf
        f = x-> 1/(1 + b - x)
    else
        f = x-> 1/(1 + e^-x)
    end
    
    return f
end

function rescale(a,b)
    
    if abs(a) < Inf && abs(b) < Inf
        f = x-> a + (b-a)*x
    elseif abs(a) < Inf
        f = x->  1/(1-x) + (a-1)
    elseif abs(b) < Inf
        f = x->  -1/x + (b+1)
    else
        f = x->-log(1/x - 1)
    end

    return f
end


"""
```
rescale(a,b,c,d)
```
Function to rescale from the interval (a,b) to the interval (c,d)

"""
function rescale(a, b, c, d)

    f = scale(a,b)
    g = rescale(c,d)

    return x->g.(f.(x))
end
