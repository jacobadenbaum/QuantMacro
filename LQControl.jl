include("Partition.jl")

abstract type LQ end

"""
```
LQProblem(Q,R,W, A, B,C, β)
```
Sets up a model for the LQ control problem

    max E_0 Σ_{t=0}^∞ β^t(X_t'*Q*X_t + u_t'*R*u_t + X_t'*W*u_t)
    st  X_{t+1} = A*X_t + B*u_t + C*ϵ_{t+1}
        X_0 given
"""
type LQProblem <: LQ
    Q
    R
    W
    A
    B
    C
    β
    P
    F
    LQProblem(Q,R,W,A,B,C,β) = new(Q,R,W,A,B,C,β)
end

"""
Normalizes an LQ problem so that we can iterate on the riccati matrix
"""
function lq_normalize(m::LQProblem)
   Q, R, W, A, B, β = m.Q, m.R, m.W, m.A, m.B, m.β

   Ã = sqrt(β)*(A - B*inv(R)*W')
   B̃ = sqrt(β)*B
   Q̃ = Q - W*inv(R)*W'

   return LQNormalized(Q̃, R, Ã, B̃)

end

"""
```
LQNormalized(Q,R, A, B,C)
```
Sets up a model for the LQ control problem

    max E_0 Σ_{t=0}^∞ X_t'*Q*X_t + u_t'*R*u_t
    st  X_{t+1} = A*X_t + B*u_t + C*ϵ_{t+1}
        X_0 given
Where here the problem has been normalized so that there are no cross
terms W and
"""
type LQNormalized <: LQ
    Q
    R
    A
    B
end

########################################################################
#################### Riccati Equation Iteration ########################
########################################################################

function iterate_riccati{T}(m::LQNormalized, P::Matrix{T})

    Q, R, A, B = m.Q, m.R, m.A, m.B

    # Compute the new P
    newP = Q + A'*P*A - A'*P*B*inv(R + B'*P*B)*B'*P*A

    # Compute the new F
    F = inv(R + B'*P*B)*B'*P*A

    return newP, F

end

function iterate_riccati(m::LQNormalized;
        rel_tol=[1e-6,1e-6], maxiter=1000)

    # Starting Value
    P = ones(size(m.Q)...)
    F̃ = ones(size(m.R,1), size(m.A, 2))
    err_P = Inf
    err_F = Inf

    iter= 0

    while (err_P > rel_tol[1] || err_F > rel_tol[2])

        iter+= 1

        newP, newF = iterate_riccati(m, P)

        err_P = norm(vec(newP - P))/norm(P)
        err_F = norm(vec(newF - F̃))/norm(F̃)

        P, F̃ = newP, newF

        if iter > maxiter
            warn("Maximum Iterations of $maxiter Exceeded")
            break
        end

    end


    return P, F̃
end

function iterate_riccati(m::LQProblem; kwargs...)

    # Normalize the LQ problem
    lqn = lq_normalize(m)

    # Solve the normalized Problem
    P, F̃ = iterate_riccati(lqn; kwargs...)

    # Unnormalize the results
    F = F̃ + inv(m.R)*m.W'

    return P,F
end

function iterate_riccati!(m::LQProblem; kwargs...)

    # Solve the riccati equations iteratively
    P, F = iterate_riccati(m; kwargs...)

    # Store it inplace
    m.P = P
    m.F = F
end

########################################################################
#################### Vaughn's Method ###################################
########################################################################

function hamiltonian(m::LQNormalized)
    # Unpack
    A, B, Q, R = m.A, m.B, m.Q, m.R

    if det(A) != 0
        # Construct Hamiltonian
        H11 = inv(A)
        H12 = inv(A)*B*inv(R)*B'
        H21 = Q*inv(A)
        H22 = Q*inv(A)*B*inv(R)*B' + A'

        H   = [ H11 H12
                H21 H22]

        return (H,)
    else
        # Make the pseudo hamiltonian
        H1  = [ A   zeros(Q)
                -Q  eye(Q)]
        H2  = [ eye(Q)      B*inv(R)*B'
                zeros(Q)  A']

        return H1, H2
    end
end

function vaughan(m::LQNormalized)
    
    # Get the Hamiltonian
    H = hamiltonian(m)

    # Construct the eigenvalues/eigenvectors
    Λ, V = eig(H...)
    
    # Sort the eigenvalues from largest to smallest and then reorder the
    # eigenvectors accordingly
    idx = sortperm(-Λ)
    Λ   = Λ[idx]
    V   = V[:,idx]
    
    # Partition the Eigenvectors and Eigenvalues
    l       = searchsortedfirst(-Λ, -1) - 1
    Λ1, Λ2  = partition(Λ, l)
    V11, V12, V21, V22 = partition(V, l)
    
    # P is chosen so that 
    #  X_{t+1} = [V11 Λ^(-1) (W_{11} + W_{12} P) 
    #            + V_{12} Λ (W_{21} + W_{22} P)] X_t
    # Is stable (where W = V^(-1))
    @show Λ, V
    P = V21*inv(V11)

    # This pins down F
    A, B, R = m.A, m.B, m.R
    F = inv(R + B'*P*B)*B'*P*A 


    return P, F
end

function vaughan(m::LQProblem)

    # Normalize the LQ problem
    lqn = lq_normalize(m)

    # Solve the normalized Problem
    P, F̃ = vaughan(lqn)

    # Unnormalize the results
    F = F̃ + inv(m.R)*m.W'

    return P,F
end

function vaughan!(m::LQ)
    m.P, m.F, = vaughan(m)
end

########################################################################
#################### Simulate System ###################################
########################################################################

function step(m::LQProblem, X, ϵ)

    # Unpack some matrices
    A, B, C, F = m.A, m.B, m.C, m.F

    # Compute the policy today
    u = -F*X

    # Compute the state tomorrow
    Xp = A*X + B*u+ C*ϵ

    return Xp, u
end
