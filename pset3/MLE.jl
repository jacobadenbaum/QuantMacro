using NLopt
using ForwardDiff 

abstract type KalmanMLE end


#= Interface for KalmanMLE Type 

In order to run MLE on a particular model, we need a type that
stores the data as well as a method to go from a vector of parameters to
a description of the state space model.  

In general, we'll use the UnrestrictedKalmanMLE type we define below,
which imposes no exclusion restrictions on the law of motion or the
observation equations.  However, we'll be able to easily restrict
certain pieces of the problem by using a different Kalman constructor
function.  

type MyKalmanMLE <: KalmanMLE
    y       # Matrix of observables, where each row is an observation
    n       # Number of columns of y
    k       # Number of latent states
    l       # The length of the parameter vector
    θ       # The true parameter vector
end

function construct_kalman{T<:Real}(m::MyKalmanMLE, θ::Vector{T})
   
    1)  Unpack the vector θ
    2)  Construct matrices T, Q, Z, H that represent the state space
        system
            α_t = T*α_{t-1} + η_t
            y_t = Z*α_t + ϵ_t
        where Var(η_t) = Q and Var(ϵ_t) = H
    
    return Kalman(T,Q,Z,H)
end

=#

########################################################################
#################### Example of Interface ##############################
########################################################################

type UnrestrictedKalmanMLE <: KalmanMLE
    y       # Matrix of observables, where each row is an observation
    n       # Number of columns of y
    k       # Number of latent states
    l       # The length of the parameter vector
    θ       # The true parameter vector
    UnrestrictedKalmanMLE(y,n,k,l) = new(y,n,k,l)
end

   
function construct_kalman(m::UnrestrictedKalmanMLE)

    θ = m.θ

    # Unpack Sizes
    n = m.n
    k = m.k
    
    used = 0
    # First k^2 are the T matrix
    T = reshape(θ[1:k^2], k,k)
    used += k^2

    # Next k^2 are the VCE of the η_t
    Q = reshape(θ[used+1:used+k^2], k,k)
    used += k^2

    # Next n x k are the Z matrix
    Z = reshape(θ[used+1:used+k*n], n,k)
    used += n*k

    # Next n x n are the H matrix
    H = reshape(θ[used+1:used+n^2],n,n)
    used += n^2

    @assert(used == length(θ), 
        "You've passed the wrong number of parameters")
    

    return Kalman(T, Q, Z, H)

end

########################################################################
#################### General Methods ###################################
########################################################################

function set_parameters!{R<:Real}(m::KalmanMLE, θ::Vector{R})
    m.θ = θ
    return Void
end

function construct_kalman!{R<:Real}(m::KalmanMLE, θ::Vector{R})
    # Set the Parameter Vector 
    m.θ = θ

    # Construct the Kalman filter
    return construct_kalman(m)
end


"""
```
likelihood(model, θ)
```
Computes the likelihood of the parameter vector θ using maximum
likelihood via the Kalman filter.

    1) Constructs a Kalman filter for the given set of parameters using
        the provided method `construct_kalman`
    2) Filters the data and computes the likelihood

"""
function log_likelihood!{R<:Real}(m::KalmanMLE, θ::Vector{R})
    
    k = construct_kalman!(m, θ)

    ll = 0
    for obs in m.y
        try
            ll += log_likelihood!(k, obs)
        catch err
            @show obs
            @show θ
            @show k
            throw(err)
        end
    end

    return ll
end

function estimate!(m::KalmanMLE, guess)
    
    # Maximize the log likelihood function
    f(x) = log_likelihood!(m, x)

    function f(x, grad) 
        if length(grad) > 0
            d = ForwardDiff.gradient(f, x)
            for i in eachindex(grad)
                grad[i] = d[i]
            end
        end
        return f(x)
    end
    q = length(guess)
    opt = Opt(:LD_LBFGS, q)
    lower_bounds!(opt, vcat(-Inf*ones(q-1), 1e-4))
    upper_bounds!(opt, Inf*ones(q))
    max_objective!(opt, f)
    minf, minx, ret = optimize(opt, guess)
    
    # Set the parameters
    set_parameters!(m, minx)
    
    # Return the estimated parameters
    return minx
end
