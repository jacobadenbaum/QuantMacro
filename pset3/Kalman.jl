#=
Title:  Kalman.jl
Author: Jacob Adenbaum
Date:   October 22, 2017

Implements a type to compute the Kalman filter
=# 

type Kalman
    T   # Law of motion for unobserved state
    Q   # VCE for unobserved state error
    Z   # Transformation to observed variable
    H   # VCE for observation errors
    α̂   # Current estimate of unobserved state
    P   # Current estimate of conditional variance of α̂
end

function Kalman(T,Q,Z,H)
    
    # Get the sizes of the relative states
    k = size(Z, 1)
    n = size(Z, 2)

    # Initialize the mean and covariance to ones
    α̂ = n==1 ? ones(eltype(T), n) :  ones(n)
    P = n==1 ? ones(eltype(T), n,n) :  ones(n,n)
    
    # Initialize the object
    return Kalman(T, Q, Z, H, α̂, P)
end

function set_state!(m::Kalman, α̂, P)
    m.α̂ = α̂
    m.P = P
    return Void
end


"""
```
filter(m::Kalman, y)
```
Updates `α̂` and `P` using the Kalman filter on the new observation `y`
"""
function filter!(m::Kalman, y)
    
    α̂, P = m.α̂, m.P
    T, Q, Z, H = m.T, m.Q, m.Z, m.H
    K = inv(Z*P*Z' + H) 
    
    m.α̂ += P*Z'*K*(y - Z*α̂)
    m.P += - P*Z'*K*Z*P
end

function forecast!(m::Kalman)
    
    # Law of Motion for unobserved state
    T, Q = m.T, m.Q

    # Filtered distribution of x_t
    α̂, P = m.α̂, m.P

    # Forecast next period
    m.α̂ = T*α̂ 
    m.P = T*P*T' + Q

end

function kalman_gain(m::Kalman)
    
    T, Q, Z, H = m.T, m.Q, m.Z, m.H
    P = m.P

    return Z*P*Z' + H
end

"""
```
log_likelihood!(m::Kalman, y)
```
Filters, forecasts, and computes the log likelihood contribution of the
observation `y`
"""
function log_likelihood!(m::Kalman, y)
     
    # Save the innovation
    v = y - m.Z*m.α̂
    
    # Compute the Kalman Gain
    F = kalman_gain(m)

    # Filter and forecast
    filter!(m, y)
    forecast!(m)
    
    # Return the log likelihood contribution
    return -1/2log(2π) - 1/2*log(det(F)) - 1/2*v'*inv(F)*v
end
