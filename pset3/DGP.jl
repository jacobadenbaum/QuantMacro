#=
Title:  DGP.jl
Author: Jacob Adenbaum
Date:   10/25/17

Tools to simulate a variety of time series processes for problem set 3
=#

using Polynomials

abstract type DGP end

"""
```
AR(α, σ)
```
Type for simulating an AR(q) process with finite lag polynomial
coefficients α (length q).  The innovation terms will have standard
deviation σ.
"""
type AR <: DGP
    α
    σ
    AR(α, σ) = begin
        lag = Poly(vcat(1, -α))
        if ! all(abs.(roots(lag)) .> 1)
            q = length(α)
            warn("This AR($q) process is not stable")
        end
        return new(α, σ)
    end
end


function rand(d::AR, n::Int; burn=100)

    # Get the lengths
    q = length(d.α)
    burn += q

    # Initialize each observation with a random innovation
    x = d.σ*randn(burn+n)

    for i=1:burn+n
        for j=1:min(i-1, q)
            x[i] += d.α[j]*x[i-j]
        end
    end

    # Drop the burn-in period
    return x[burn+1:end]
end

type MA <: DGP
    α
    σ
end

function rand(d::MA, n::Int; burn=100)
    
    # Get the lengths
    q = length(d.α)
    burn += q

    # Get the sequence of shocks
    ϵ = d.σ*randn(burn + n)
    x = zeros(burn + n)

    for i=1:burn+n
        for j=1:min(i, q)
            x[i] += d.α[j]*ϵ[i+1-j]
        end
    end

    return x[burn+1:end]
end


########################################################################
#################### Time Series Tools #################################
########################################################################

function lag(x, l)

    lagged = similar(x)

    for i in eachindex(x)
        if i <= l
            lagged[i] = NaN
        else
            lagged[i] = x[i-l]
        end
    end

    return lagged
end

dropna{T}(x::Vector{T}) = x[.!isnan.(x)]

function dropna{T}(x::Matrix{T})

    n, k = size(x)
    xp   = x'
    idx  = trues(n)
    for i=1:n
        idx[i] = !any(isnan.(xp[:,i]))
    end

    return x[idx,:]
end

autocor(x, l::Int) = cor(dropna(hcat(x, lag(x,l))))[1,2]
autocor(x,l) = [autocor(x,lag) for lag in l]
