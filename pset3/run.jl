include("DGP.jl")
include("Kalman.jl")
include("MLE.jl")

########################################################################
#################### Problem 1: AR(1) ##################################
########################################################################

# Make an AR(1) process using innovations with variance 1, and
# autocorrelation of 1.
d = AR(.7, 3)
x = rand(d, 1000)

# Make a type for AR(1) MLE with Kalman Filter

type AR_MLE <: KalmanMLE
    y       # Matrix of observables, where each row is an observation
    θ       # The true parameter vector
    AR_MLE(y) = new(y)
end

function construct_kalman(m::AR_MLE)
    θ = m.θ 
    ρ = θ[1:end-1]
    σ = θ[end]
    
    # Number of lags we need
    q = length(ρ)
    
    # Transition for latent state
    T = vcat(ρ',eye(q)[1:q-1,:])
    Q = Diagonal(vcat(σ, zeros(q-1)))

    # Transition for the observed State
    Z = hcat(1, zeros(1,q-1))
    H = 0
    
    return Kalman(T,Q,Z,H)
end

# Try to recover the data
k = AR_MLE(x)
ρ̂ = estimate!(k, [.5, .5])



########################################################################
#################### Problem 2: AR(k) ##################################
########################################################################

# Make an AR(1) process using innovations with variance 1, and
# autocorrelation of 1.
ρ = [-.75, .2, .37]
d2 = AR(ρ, 1)
x2 = rand(d2, 1000)

# Try to recover the data
k2 = AR_MLE(x2)
guess = ones(length(ρ)+1)*.5
ρ̂ = estimate!(k2, guess)

