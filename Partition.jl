#=
Name:   Partition.jl
Author: Jacob Adenbaum
Date:   10/18/2017

Some helper code to partition vectors and matrices into pieces
=#

"""
```
partition(x::Vector, indices)
```
Partition a vector into pieces based on the given indices
"""
function partition{T}(x::Vector{T}, indices)
    
    part    = []
    idx     = vcat(indices)
    for i in eachindex(idx)
        if i == 1
            push!(part, x[1:idx[i]])
        else
            push!(part, x[idx[i-1]+1:idx[i]])
        end
    end
    
    # Push the last piece of the partition
    push!(part, x[idx[end]+1:end])

    return part
end


"""
```
partition(x::Matrix, indices)
```
Partition a matrix into a collection of block submatrices based on the given
indices
"""
function partition{T}(X::Matrix{T}, idx)
    
    # List of partition pieces
    part = []
    
    l   = length(idx)
    for i = 1:l+1, j=1:l+1 
        
        istart  = i==1      ? 1         : idx[i-1]+1
        istop   = i==l+1    ? size(X,1) : idx[i]  
        jstart  = j==1      ? 1         : idx[j-1]+1
        jstop   = j==l+1    ? size(X,2) : idx[j]  
        piece   = X[istart:istop, jstart:jstop]
        push!(part, piece)
    end

    return part
end
