# Load code for general grids

module Interpolate

include("Grid.jl")

using Interpolations
import Interpolations.scale, Interpolations.gradient,
    Interpolations.gradient!,Interpolations.hessian,
    Interpolations.hessian! 
import Base.getindex
import  Base.getindex, Base.start, Base.next, Base.done, Base.length,
        Base.push!, Base.size


export LinearInterpolater, CubicSpline, make_grid!, evaluate!, estimate!

abstract type Interpolater end

"""
```
evaluate!(itp, f)
```
Evaluates the function f at every point on the grid of the Interpolater
itp, and stores it as an array in the shape of itp.grid as itp.values.
"""
function evaluate!(itp::Interpolater, f::Function)
    # Dimensions of grid
    dims = size(itp.grid)

    # Loop through the grid and evaluate the function
    values = [f(x) for x in itp.grid]
    itp.values = reshape(values, dims...)
end

function make_grid!(itp::Interpolater, m)
    # Make a grid with the appropriate number of endpoints
    itp.grid = Grid()
    for (a,b) in zip(itp.lower, itp.upper)
        push!(itp.grid, linspace(a,b,m))
    end
    return itp
end

function estimate!(itp::Interpolater)
    unscaled = construct(itp)
    itp.itp = scale(unscaled, itp.grid.points...)
    itp.itp = extrapolate(itp.itp, Linear())
end

# Get the linear interpolation using indexing
getindex(itp::Interpolater, x...) = itp.itp[x...]


# Support Passing the Gradient and Hessian
gradient(itp::Interpolater, args...) = gradient(itp.itp, args...)
gradient!(g, itp::Interpolater, args...) = gradient!(g,itp.itp, args...)
hessian(itp::Interpolater, args...) = hessian(itp.itp, args...)
hessian!(g, itp::Interpolater, args...) = hessian!(g,itp.itp, args...)


########################################################################
#################### Linear Interpolation ##############################
########################################################################

type LinearInterpolater <: Interpolater
    lower   # Set of lower Bounds
    upper   # Set of upper
    grid    # The Grid Edges
    values  # The Values
    itp     # The Interpolater
    LinearInterpolater(lower, upper) = new(lower, upper)
end

function LinearInterpolater(lower, upper, f::Function; m=100,
    extrapolate=true)
    
    itp = LinearInterpolater(lower, upper)
    make_grid!(itp, m)
    evaluate!(itp, f)
    estimate!(itp)  
    return itp
end

# Construct the actual interpolater object, with linear options
function construct(itp::LinearInterpolater)
    unscaled = interpolate(itp.values, BSpline(Linear()), OnGrid())
    return unscaled
end

########################################################################
#################### Cubic Spline Interpolation ########################
########################################################################

type CubicSpline <: Interpolater
    lower   # Set of lower Bounds
    upper   # Set of upper
    grid    # The Grid Edges
    values  # The Values
    itp     # The Interpolater
    CubicSpline(lower, upper) = new(lower, upper)
end

function CubicSpline(lower, upper, f::Function; m=100,
    extrapolate=true)
    
    itp = CubicSpline(lower, upper)
    make_grid!(itp, m)
    evaluate!(itp, f)
    estimate!(itp)  
    return itp
end

# Construct the 
function construct(itp::CubicSpline)
    unscaled = interpolate(itp.values, BSpline(Cubic(Natural())), OnGrid())
    return unscaled
end

end

using Interpolate
