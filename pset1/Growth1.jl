
using   Optim, Distributions, FastGaussQuadrature, DataFrames, Calculus,
        NLopt

include("../Models.jl")
include("../LQControl.jl")
include("../Interpolater.jl")

import QuadGK.quadgk
import Calculus.gradient

########################################################################
#################### Growth Problem ####################################
########################################################################

abstract type Growth1 <: Model end

function set_bounds!(m::Growth1, lower, upper)

    # Compute Variance of the errors
    σ = m[:σ]
    ρ = m[:ρ]
    σz = sqrt(σ^2/(1-ρ^2))

    # Grid of productivity shocks should be 3.5 times larger than the
    # standard deviation of z
    m.lower = vcat(lower, -3.5σz)
    m.upper = vcat(upper, 3.5σz)
end

nstates(m::Growth1) = 2
ncontrols(m::Growth1) = 2
nshocks(m::Growth1) = 1


# Returns bounds on the control variables
function control_bounds(m::Growth1)
    lower = [0.0, 0.0]
    upper = [1.0, 10.0]

    return lower, upper
end


"""
```
flow(model, states, controls)
```
Computes the flow utility of the Growth1 model, given a vector of states
and controls.  Calls ``solve_foc`` in order to compute present-period
consumption and investment given the choice of the control variables.

Note that since the static problem must be handled separately depending
on whether or not elasticity of labor is 0 or not, the interpretation of
the control variable will differ.  If m[:ψ] is zero, then the control
variable will be the share of output used for consumption.  If m[:ψ] is
nonzero, then it will be hours worked.
"""
function flow(m::Growth1, states, controls)
    
    # Unpack the Arguments
    c, h, kp, k, z = solve_focs(m, states, controls)

    # Compute the flow utility
    return U(m, c, h)
end

function solve_focs(m::Growth1, states, controls)
    # Unpack Parameters
    θ  =m[:θ]  # Labor Share
    ψ  =m[:ψ]  # Preference for leisure
    γz =m[:γz] # Productivity Growth
    γn =m[:γn] # Productivity Growth
    δ  =m[:δ]  # Depreciation Rate

    # Unpack the arguments
    k, z = states
    h, kp = controls
    
    # Compute Tomorrow's Consumption
    Y = F(m, k, exp(z), h)
    c = Y + (1-δ)*k -(1+γz)*(1+γn)*kp
    
    return c, h, kp, k, z
end

"""
```
motion(model, states, controls, shocks)
```
Law of motion governing the evolution of the states of the system.
"""
function motion(m::Growth1, states, controls, shocks)

    # Unpack the arguments
    c, h, kp, k, z = solve_focs(m, states, controls)

    # There's only one shock (but it could get passed as a vector)
    ϵ = sum(shocks)

    # Get productivity tomorrow given the states, controls,
    # and shocks
    zp = m[:ρ]*z + ϵ
    
    # Compute Capital Tomorrow
    # kp = (F(m, k, z, h) + (1-m[:δ])k - c)/((1+m[:γn])*(1+m[:γz]))


    # Return the stacked vector of states tomorrow
    return vcat(kp, zp)

end

# Model Specific Guess
steady_state(m::Growth1; kwargs...) = begin
    guess = ones(2*nstates(m) + ncontrols(m))*.5
    return steady_state(m, guess; kwargs...)
end
