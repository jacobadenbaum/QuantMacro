include("Growth1.jl")

type Bellman1 <: Growth1
    # Parameters to be set
    parameters::Dict
    ## Need to Choose
    ## β    # Discount Factor
    ## θ    # Labor Share
    ## ψ    # Preference for leisure
    ## γz   # Productivity Growth
    ## γn   # Population Growth
    ## δ    # Depreciation Rate
    ## ρ    # Autocorrelation of shocks
    ## σ    # Std Deviation of shocks
    ## U    # U(c, h) Flow utility
    # Bounds
    lower
    upper
    # Equilibrium objects
    value
    policy
    # Convergence flag
    converged
    err

    Bellman1(parameters) = new(parameters)
end

function Bellman1(;kwargs...)

    # Put the parameters in a dict
    parameters = Dict(kwargs)

    return Bellman1(parameters)

end

########################################################################
#################### Value Function Iteration ##########################
########################################################################


function prepare_bellman!(m::Bellman1)

    # Construc the value functions
    m.value  = LinearInterpolater(m.lower, m.upper, x->1/sum(sqrt.(x)); m=50)
    m.policy = LinearInterpolater(m.lower, m.upper, x->0.0; m=50)

    # Compute the Quadrature weights *once*
    m[:nodes], m[:weights] = gausshermite(20)

    return m
end


"""
```
solve_foc(m::Growth1, k, z, h)
```
Given a Growth1 instance, a capital level, a productivity level, and a
search term (scaled between 0 and 1).  If m[:ψ] is 0.0, then this treats h
as the percentage of output which will be devoted to consumption.  If
m[:ψ] is 1, it treats h as hours worked.

This function returns a tuple (c, h, kp) where
    c: the level of consumption
    h: the hours worked
    kp: capital in the next period
are implied by the choice of (k, z, h).
"""
function solve_foc(m::Growth1, k, z, h)
    θ  =m[:θ]  # Labor Share
    ψ  =m[:ψ]  # Preference for leisure
    γz =m[:γz] # Productivity Growth
    γn =m[:γn] # Productivity Growth
    δ  =m[:δ]  # Depreciation Rate

    # What happens today (c derived from intratemporal first order
    # condition between consumption and leisure)
    # @show θ, ψ, k, z, h, θ
    if ψ > 0
        c = θ/ψ*k*z^(1-θ)*h^θ*(1-h)

        # This implies what capital tomorrow will be
        kp = (k^θ*(z*h)^(1-θ) + (1-δ)k - c)/(1+γn)/(1+γz)
    else
        # Reinterpret h as searching along the consumption grid

        # Production
        Y = k^θ*z^(1-θ)
        c = h*Y # Scale the h up to be a fraction of total output
        h = 1.0 # they don't care about leisure => labor is inelastic

        # Capital Tomorrow
        kp = (k^θ*(z*h)^(1-θ) + (1-δ)k - c)/(1+γn)/(1+γz)
    end

    return c, h, kp

end

# Objective function for the Bellman equation
function recursive_objective(m::Growth1, k, z, h)
    # Readability
    β = m[:β]   # Discount Factor
    γn= m[:γn]  # Population Growth
    ρ = m[:ρ]   # Autocorrelation of shocks
    σ = m[:σ]   # Std Deviation of shocks

    # Solve the problem for c and k' given h
    c, h, kp = solve_foc(m, k, z, h)

    # @show kp
    function EV(x)
        # @show x
        zp = exp(ρ*log(z) + x)
        # @show kp, zp
        return m.value[kp, zp]
    end

    if kp < (1-m[:δ])*k
        return -1e100
    end

    # The Value Today
    today = U(m, c, h)

    # Use pre-computed quadrature weights
    nodes   = m[:nodes]
    weights = m[:weights]

    # Integrate to get the value tomorrow
    nodes = nodes*sqrt(2)*m[:σ]
    weights = weights/sqrt(pi)
    tomorrow  = dot(weights, EV.(nodes))

    # Random checks on the fast quadrature algorithm
    if randn() > 4.0
        ϵ = Normal(0, m[:σ])
        tomorrow_stable, err  = quadgk(x->EV(x)*pdf(ϵ, x),-Inf, Inf)
        if abs(tomorrow - tomorrow_stable) > 1e-4
            warn("Fast Quadrature Not Working")
            @show c, h, k, z, kp
        end
    end

    return today + β*(1+γn)*tomorrow
end

function converged(m::Growth1, new_values, new_policy; tol=1e-6)
    # Code Here
    # Returns true if converged, false if not

    err = m.value.values - new_values.values
    m.err = maximum(abs.(err))

    if m.err < tol
        return true
    else
        return false
    end
end

########################################################################
#################### Simulate the Model ################################
########################################################################


"""
```
step(m::Growth1, k, z; stochastic)
```
Function to step a simulation of the model forward by one step.
Given a value of capital k and productivity z, calls solve_foc to
compute consumption, hours, and next period capital, and then generates
the next period's productivity.

Set stochastic=true for simulations of the model including continued
stochastic components.

Set stochastic=false for computation of impulse response functions: this
will trace out the economy deterministically given an initial shock to
the states
"""
function step(m::Bellman1, state; stochastic=true)
    # Unpack the state 
    k,z = state
    
    # Given a level of capital k and productivity z, compute
    # consumption, hours, and tomorrow's capital
    c, h, kp = solve_foc(m, k, z, m.policy[k,z])

    if stochastic
        # Compute the realization of the productivity shock, which
        # follows the process:
        #           log(z') = ρ log(z) + ϵ
        innovations = Normal(0, m[:σ])
        ϵ = rand(innovations)
        zp = exp(ϵ)*z^(m[:ρ])
    else
        zp = z^(m[:ρ])
    end

    # Create a dictionary of today's values
    today = DataFrame(
        Capital = k,
        Productivity= z,
        Consumption = c,
        Hours = h,
        Output = F(m, k, z, h))

    # Return the values today, and the state tomorrow
    return today, (kp, zp)
end

