include("../LQControl.jl")

########################################################################
#################### LQ Approximation ##################################
########################################################################

type LQ1 <: Growth1
    # Parameters to be set
    parameters::Dict
    ## Need to Choose
    ## β    # Discount Factor
    ## θ    # Labor Share
    ## ψ    # Preference for leisure
    ## γz   # Productivity Growth
    ## γn   # Population Growth
    ## δ    # Depreciation Rate
    ## ρ    # Autocorrelation of shocks
    ## σ    # Std Deviation of shocks
    ## U    # U(c, h) Flow utility
    # Bounds
    lower
    upper
    # Computed LQ Approximation
    LQ 
    LQ1(parameters) = new(parameters)
end

function LQ1(;kwargs...)

    # Put the parameters in a dict
    parameters = Dict(kwargs)

    lq = LQ1(parameters)

    # Renormalize the discount factor
    lq[:β] *= (1+lq[:γn])
    
    return lq

end



