using PyPlot
include("Growth1.jl")
include("LQ1.jl")

"""
```
plot_normalized(df, xsymbol, ysymbol, options)
```

Wrapper to plot a series normalizing its starting value to 1.
Pulls the x and y series from the dataframe df, using xsymbol and
ysymbol to index.

Passes all other arguments through to PyPlot
"""
function plot_normalized(df, xsymbol, ysymbol, args...; kwargs...)

    xseries = df[xsymbol]
    yseries = df[ysymbol]/df[ysymbol][1]

    fig, ax = gcf(), gca()
    ax[:plot](xseries, yseries, args...; kwargs...)
end

########################################################################
#################### Parameter Set 1 ###################################
########################################################################

println("Estimating Value function for Parameter Set 1")

# Define Utility and Production Functions
U(m::Growth1, c, h) = m[:ψ] == 0 ? log(c) : log(c) + m[:ψ]*log(1-h)
F(m::Growth1, k, z, h) = k^m[:θ]*(z*h)^(1-m[:θ])

#Setup Basic Growth Model with parameter set 1
m1 = LQ1(
        β=.95,
        θ=1/3,
        ψ=1.7,
        γz=0.05,
        γn=0.03,
        δ=0.1,
        ρ=0.9,
        σ=.1)
set_bounds!(m1, 0, 10)
LQApproximate!(m1)



# Setup the value function
# prepare_bellman!(m1, 1e-3, 10)

# Iterate the Bellman Equation
# iterate_bellman!(m1)

# Simulate the model over lots of periods
# history1 = simulate(m1, [5, 1]; T=100)
