This repository holds the code for Jacob Adenbaum's coursework in UMN Econ 8185 
(Advanced Macroeconomics). The official repository can be found on Gitlab at
https://gitlab.com/jacobadenbaum/QuantMacro/

This README file is incomplete.  I'll fill in more details documenting
the structure of the code soon.  
