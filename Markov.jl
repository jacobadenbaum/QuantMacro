#=
Title:  Markov.jl
Author: Jacob Adenbaum
Date:   9/10/17

This code provides a basic framework for approximating a continuous
markov random variable as a discrete markov chain
=#

using Distributions
import Distributions.Distribution
import Base.mean, Base.var
abstract type DGP end

# An AR(1) Process
type AR1 <: DGP
    ρ::Float64
    ϵ::Distribution
end

# Finite Sample Properties
mean(m::AR1) = mean(m.ϵ)
var(m::AR1)  = var(m.ϵ)/(1-m.ρ^2)

"""
```
z' = next(m::AR1, z, args...)
```
Computes a random next value in the process given that the current value
is z.  

args can be used to specify dimension arguments which will be passed to
the rand function.
"""
function next(m::AR1, z, args...)
    return m.ρ*z + rand(m.ϵ, args...)
end


"""
```
sample(m::AR1, n::Int, [z0])
```
Computes a sample of the specified AR1 process of length n.  If z0 is
specified, the first observation will be set to z0.  Otherwise, a random
initialization will be drawn from the unconditional distribution of the
process.  
"""
function sample(m::DGP, n::Int, z0)
    
    # Initialize Vector
    s = Vector{Float64}(n)
    s[1] = z0 
    for i=2:n
        s[i] = next(m, s[i-1])
    end
    
    return s
end

# If we didn't specify z0
function sample(m::DGP, n::Int)
    # Randomly initialize
    z0 = mean(m) + var(m)*randn()   # Random starting point
    
    # Run over the sample
    return sample(m, n, z0)
end


########################################################################
############# Approximating a DGP with a Markov Chain ##################
########################################################################

type Markov
    states                  # Values of each state
    P::Array{Float64, 2}    # Transition Matrix
    Markov(states, P) = begin
        
        # Check that the list of states is sorted
        @assert(issorted(states), "List of states must be sorted")

        # Check that the transition matrix is the right size
        n = length(states)
        @assert(all(size(P).==(n, n)), "Transition matrix must be $n x $n")

        # Check that the transition matrix is markov
        @assert(all(abs(sum(P,2) - 1) .< 1e-4), 
            "Rows of the transition matrix P must sum to 1")
        
        return new(states, P)
    end
end

"""
```
approximate(m::DGP; nstates, k, sample_size)
```

Approximates the continuous support data generating process using a
markov process with the specified number of states, which will be evenly
distributed over the the interval:
    [mean(m) - k*var(m), mean(m) + k*var(m)]

sample size controls the number of random samples we will use to
estimate the transition probabilities. (Default is 100,000)
"""
function approximate(m::DGP; nstates=100, k=3, sample_size=100000)
    
    # Construct the grid over ``most'' of the support of m
    lower  = mean(m) - k*var(m)
    upper  = mean(m) + k*var(m)
    states = linspace(lower, upper, nstates)
    
    # Calculate the transition probabilities state by state
    P = zeros(nstates, nstates)
    for (i, state) in enumerate(states)
        # Compute tomorrow's state lots of times
        zp = next(m, state, sample_size)
        
        # Figure out which state they go into
        idx = map(x->searchsortedfirst(states, x), zp)

        # Truncate the top state
        idx = minimum(idx, nstates)

        # Compute the average
        for j=1:nstates
            P[i, j] = sum(idx .== j)/sample_size
        end
    end

    return states, P
end

