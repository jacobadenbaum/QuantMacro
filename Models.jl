include("Newton.jl")

import Base.getindex, Base.setindex!, Base.show

abstract type Model end

# Example Interface
# type ExampleModel <: Model
#     # Any model parameters get stored in a lookup table
#     parameters::Dict{Symbol, Any}
#
#     # You could put any other fields here that you want if you need them
#     # for your specific implementation
#     #
#     #
#     #               (Your Fields Here!)
#     #
#     #
# end

# Indexing just looks up parameters
getindex(m::Model, key) = m.parameters[key]
getindex(m::Model, keys::AbstractArray) = [m[key] for key in keys]

function setindex!(m::Model, val, key)
    m.parameters[key] = val
    return m
end

# Print the model nice and pretty
function show(io::IO, x::Model)
    T = typeof(x)
    out = "$T("
    for key in keys(x.parameters)
        out *= ":$key=$(x[key])"
    end
    out *= ")"
    print(io, out)
end

########################################################################
#################### Value Function Iteration ##########################
########################################################################

function iterate_bellman!(m::Model)

    # Construct Bellman-Compatible Value Functions if it's not defined
    if ! isdefined(m, :value)
        prepare_bellman!(m)
    end

    # Iterate the value functions until convergence
    itercount = 0
    m.converged = false
    while ! m.converged
        itercount += 1
        println("Iteration $itercount. ")
        iterate!(m)
        println("Error is $(m.err).")
    end

    return m
end

function iterate!(m::Model)

    # Iterate on the value function for each agent
    values, policy = update(m)

    # Check whether we've converged and save the new value/policy
    # functions
    m.converged = converged(m, values, policy)
    m.value  = values
    m.policy = policy

end

# Update the value function
function update(m::Model)

    # Make a copy
    value  = deepcopy(m.value)
    policy = deepcopy(m.policy)
    for (i, s) in enumerate(m.value.grid)

        print("\rUpdating Point $i/$(length(m.value.grid))")

        # Get the bounds
        opt = optimize(h->-recursive_objective(m, s..., h), 1e-3, 1-1e-3)

        value.values[i] = -opt.minimum
        policy.values[i]= opt.minimizer

    end
    print("\n")

    estimate!(value)
    estimate!(policy)
    return value, policy
end

########################################################################
#################### Compute Steady State ##############################
########################################################################

"""
```
steady_state_foc(model)
```
Computes the steady state of the model for use in constructing a
quadratic expansion of the problem.
"""
function steady_state_foc(m::Model, states, controls, λ)

    # Check if any of the multipliers are infinite, and pretend that
    # they're zero if they are (these constraints will always bind)
    idx = find(isinf, λ)
    λ[idx] = 0

    # In the steady state, there are no shocks
    shocks = zeros(nshocks(m))

    ## First order conditions with respect to controls
    # Derivative of flow utility with respect to the controls
    drdu = gradient(x->flow(m, states, x), controls)
    dgdu = jacobian(x->motion(m, states, x, shocks), controls, :central)
    
    eq1 = drdu + dgdu'*λ

    ## First order conditions with respect to states
    drdX = gradient(x->flow(m, x, controls), states)
    dgdX = jacobian(x->motion(m, x, controls, shocks), states, :central)

    eq2 = m[:β]*drdX - λ + m[:β]*dgdX'*λ

    ## First order condition with respect to λ (i.e., the states must
    ## stay constant following the law of motion)
    eq3 = states - motion(m, states, controls, shocks)

    vals = vcat(eq1, eq2, eq3)
end

function steady_state_foc(m::Model, x::Vector)
    # Number of states and controls
    idx = nstates(m) + ncontrols(m)

    # Unpack the states and the controls
    states, controls = unpack(m, x[1:idx])

    # Last one is the lagrange multiplier
    λ = x[idx+1:end]

    vals = steady_state_foc(m, states, controls, λ)
end

function steady_state(m::Model, guess; λ=false)

    # Get Bounds on the state
    state_lower = m.lower
    state_upper = m.upper

    # Get bounds on the controls
    controls_lower, controls_upper = control_bounds(m)

    # No upper bounds on the λ bounds
    λ_lower = zeros(nstates(m))
    λ_upper = Inf*ones(nstates(m))

    # Put the bounds together
    lower = vcat(state_lower, controls_lower, λ_lower) + 1e-3
    upper = vcat(state_upper, controls_upper, λ_upper) - 1e-3


    function focs(x)
        # Compute the values
        vals = steady_state_foc(m, x)

        return vals
    end

    # steady = fzero(focs, guess)

    function focs!(x, fvec)
        val = focs(x)
        grad = gradient(focs, x)

        for i in eachindex(grad)
            fvec[i] = grad[i]
        end

        println(val)
        return val
    end

    steady, res, flag = fzero(focs, guess, lower, upper)

    # Set up optimization
    # opt = Opt(:LD_LBFGS, 2*nstates(m) + ncontrols(m))
    # lower_bounds!(opt, lower)
    # upper_bounds!(opt, upper)
    # xtol_abs!(opt,1e-30)

    # min_objective!(opt, focs!)
    # val, minimizer, ret = NLopt.optimize(opt, guess)

    # Set up the optimization problem
    if flag == 0
        if λ
            return steady
        else
            return steady[1:nstates(m) + ncontrols(m)]
        end
    else
        throw(AssertionError("""
            Guess of $guess is bad.
            Fzero returns steady, res, flag = ($steady, $res, $flag)
            """))
    end
end

"""
```
unpack(model, x)
```
Unpacks a general vector containing a stacked representation of the
states, controls, and shocks of the system into a form that the flow and
motion functions can use.
"""
function unpack(m::Model, x)

    id1 = nstates(m)
    id2 = id1 + ncontrols(m)
    id3 = id2 + nshocks(m)

    states = x[1:id1]
    controls = x[id1+1:id2]

    if length(x) == id2
        return states, controls
    elseif length(x) == id3
        shocks = x[id2+1:end]
        return states, controls, shocks
    else
        throw(AssertionError("Cannot unpack x=$x: wrong length"))
    end
end

# Handle passing states, controls, and shocks as a big vector
flow(m, x::Vector) = flow(m, unpack(m, x)...)
motion(m, x::Vector) = motion(m, unpack(m, x)...)

########################################################################
#################### LQ Approximation ##################################
########################################################################
function LQApproximate(m::Model)

    # Compute the steady state of the system
    ss = steady_state(m)

    # Compute Quadratic Approximation of Flow Utility
    Q, R, W = QuadraticApprox(m, flow, ss)

    # Compute Linear Approximation of the Law of Motion
    A, B, C = LinearApprox(m, motion, vcat(ss, zeros(nshocks(m))))

    return LQProblem(Q, R, W, A, B, C, m[:β])
end

function LQApproximate!(m::Model)

    # Make the LQ Approximation
    lq = LQApproximate(m)

    # Store it in the correct field
    m.LQ = lq

end
"""
```
QuadraticApprox(model, f, ss)
```

Computes a quadratic approximation of the real valued function f about
the steady state ss.  Returns matrices Q, R, W such that we can write this
quadratic approximation as

    f(X, u) = X'*Q*X + u'*R*u + 2 X'*W*u

In other words, we need to expand the states X and the controls u to
include a constant term.
"""
function QuadraticApprox(m::Model, f::Function, ss)

    # Unpack the steady State
    xss, uss = unpack(m, ss)

    # Compute Quadratic Approximation of flow utilty around the steady
    # state
    J = Calculus.gradient(x->f(m,x), ss)
    H = Calculus.hessian(x->f(m,x), ss)
    @show H, J

    k,l = nstates(m), ncontrols(m)

    # Repartition the Matrix
    M11 = flow(m, ss) - J'*ss + 1/2*ss'*H*ss
    M21 = 1/2*(J - H*ss)
    M12 = M21'
    M22 = 1/2*H

    M = [M11 M12;
         M21 M22]

    
    Q = M[1:k+1, 1:k+1]
    R = M[k+2:end, k+2:end]
    W = M[1:k+1, k+2:end]
    
    return Q, R, W

end

"""
```
LinearApprox(model, f, ss)
```

Computes a linear approximation of the real valued function f about
the steady state ss.  Returns matrices A, B, C such that we can write this
quadratic approximation as

    f(X, u, ϵ) = A*X + B*u + C*ϵ

In other words, we need to expand the states X, controls u, and shocks
ϵ to include a constant term.
"""
function LinearApprox(m::Model, f::Function, ss)
    # Unpack the steady State
    xss, uss, ϵss = unpack(m, ss)

    # Compute Linear Approximation of f around the steady
    # state
    Jxs, Jus, Jϵs = [], [], []
    for i=1:nstates(m)
        # Compute the Jacobian
        J = Calculus.gradient(x->f(m, x)[i], ss)
        # Unpack it into the contribution of each piece (states,
        # controls, shocks...)
        Jxi, Jui, Jϵi = unpack(m, J)

        # Store each piece
        push!(Jxs, Jxi)
        push!(Jus, Jui)
        push!(Jϵs, Jϵi)
    end

    # Put all of them back together and transpose to get the shape
    # right:
    #   The (i,j) entry should be the derivative of f(m,x)[i] with
    #   respect to x[j]
    Jx = hcat(Jxs...)'
    Ju = hcat(Jus...)'
    Jϵ = hcat(Jϵs...)'

    # Assemble the expanded matrices
    A11 = 1
    A12 = zeros(1, nstates(m))
    A21 = f(m, ss)-Jx*xss-Ju*uss-Jϵ*ϵss
    A22 = Jx
    A = [A11 A12
         A21 A22]

    B12 = zeros(1, ncontrols(m))
    B22 = Ju
    B = [B12
         B22]

    C12 = zeros(1, nshocks(m))
    C22 = Jϵ
    C = [C12
         C22]

    return A, B, C
end


########################################################################
#################### Simulate the Model ################################
########################################################################

function simulate(m::Model, s; T=100, kwargs...)

    # Infer the number of states from the starting value
    d = length(s)
    history = DataFrame()

    # Compute a new step each time
    for t=1:T

        # Compute this period's aggregate values
        new_record, s  = step(m, s; kwargs...)
        new_record[:t] = t

        # Add the new period to the big dataframe
        history = vcat(history, new_record)
    end

    return history
end

