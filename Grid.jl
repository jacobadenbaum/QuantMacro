########################################################################
#################### Grid Implementation ###############################
######################################################################## 

import  Base.getindex, Base.start, Base.next, Base.done, Base.push!,
        Base.size, Base.length

type Grid
    points
    dims::Vector{Int}
    Grid() = new([],[])
end

function Grid(points::Matrix)
    g = Grid()
    g.points= [points[:,i] for i=1:size(points,2)]
    g.dims  = [length(p) for p in g.points]
    return g
end

function Grid(points...)
    g = Grid()
    g.points= [points...]
    g.dims  = [length(p) for p in g.points]
    return g
end

# Indexing Rule
function index(dims, s, k)
    if s > 1
        d = prod(dims[1:s-1])
        first = index(dims, s-1, mod(k-1, d)+1) 
        last  = ceil(Int, k/d)
        return vcat(first, last)
    elseif s == 1
        return [k]
    else
        throw(AssertionError("s cannot be less than 1"))
    end
end

function getindex(g::Grid, idx::Int)
    s = length(g.dims)
    return g[index(g.dims,s,idx)]
end

# Treat multiple integers as a vector
getindex(g::Grid, idx...) = g[[idx...]]

function getindex(g::Grid, idx::Vector)
    l = length(idx)
    k = length(g.dims)
    @assert(l  == k, 
        "Index has dimension $l but the grid has dimension $k")
    
    # Get the point from the list of points
    point = [g.points[d][i] for (d,i) in enumerate(idx)]
    if length(point) > 1
        return point
    else
        return point[1]
    end
end


## Make the grids iterable
# start(g::Grid) = ones(Int, length(g.dims))
start(g::Grid) = 1 

function next(g::Grid, idx)
    # Get the current value
    val = g[idx]
    return val, idx+1
    
    # This Code Doesn't Work
    # for (i,k) in enumerate(idx)
    #     if k < g.dims[i] || i == length(g.dims)
    #         idx[i] += 1
    #         return (val, idx)
    #     elseif idx[i+1] < g.dims[i+1]
    #         idx[i] = 1
    #         idx[i+1] += 1
    #         return (val, idx)
    #     end
    # end
end

# done(g::Grid, idx) = any(idx .> g.dims)
done(g::Grid, idx) = idx > prod(g.dims)

length(g::Grid) = prod(g.dims)
size(g::Grid)   = g.dims
dim(g::Grid)    = length(g.dims)

## Add new dimensions to the grid
function push!(g::Grid, x)
    push!(g.points, x)
    push!(g.dims, length(x))
    return g
end

## Plotting Grids
plot_grid(g::Grid, dim) = reshape(map(x->x[dim], g), g.dims...)

